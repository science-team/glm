Description: Various fixes to manual.md
Author: Andrea Pappacoda <andrea@pappacoda.it>
Forwarded: https://github.com/g-truc/glm/pull/1120
Last-Update: 2022-09-17

--- glm-0.9.9.8+ds.orig/manual.md
+++ glm-0.9.9.8+ds/manual.md
@@ -8,105 +8,106 @@
 <div style="page-break-after: always;"> </div>
 
 ## Table of Contents
-+ [0. Licenses](#section0)
-+ [1. Getting started](#section1)
-+ [1.1. Using global headers](#section1_1)
-+ [1.2. Using separated headers](#section1_2)
-+ [1.3. Using extension headers](#section1_3)
-+ [1.4. Dependencies](#section1_4)
-+ [1.5. Finding GLM with CMake](#section1_5)
-+ [2. Preprocessor configurations](#section2)
-+ [2.1. GLM\_FORCE\_MESSAGES: Platform auto detection and default configuration](#section2_1)
-+ [2.2. GLM\_FORCE\_PLATFORM\_UNKNOWN: Force GLM to no detect the build platform](#section2_2)
-+ [2.3. GLM\_FORCE\_COMPILER\_UNKNOWN: Force GLM to no detect the C++ compiler](#section2_3)
-+ [2.4. GLM\_FORCE\_ARCH\_UNKNOWN: Force GLM to no detect the build architecture](#section2_4)
-+ [2.5. GLM\_FORCE\_CXX\_UNKNOWN: Force GLM to no detect the C++ standard](#section2_5)
-+ [2.6. GLM\_FORCE\_CXX**: C++ language detection](#section2_6)
-+ [2.7. GLM\_FORCE\_EXPLICIT\_CTOR: Requiring explicit conversions](#section2_7)
-+ [2.8. GLM\_FORCE\_INLINE: Force inline](#section2_8)
-+ [2.9. GLM\_FORCE\_ALIGNED\_GENTYPES: Force GLM to enable aligned types](#section2_9)
-+ [2.10. GLM\_FORCE\_DEFAULT\_ALIGNED\_GENTYPES: Force GLM to use aligned types by default](#section2_10)
-+ [2.11. GLM\_FORCE\_INTRINSICS: Using SIMD optimizations](#section2_11)
-+ [2.12. GLM\_FORCE\_PRECISION\_**: Default precision](#section2_12)
-+ [2.13. GLM\_FORCE\_SINGLE\_ONLY: Removed explicit 64-bits floating point types](#section2_13)
-+ [2.14. GLM\_FORCE\_SWIZZLE: Enable swizzle operators](#section2_14)
-+ [2.15. GLM\_FORCE\_XYZW\_ONLY: Only exposes x, y, z and w components](#section2_15)
-+ [2.16. GLM\_FORCE\_LEFT\_HANDED: Force left handed coordinate system](#section2_16)
-+ [2.17. GLM\_FORCE\_DEPTH\_ZERO\_TO\_ONE: Force the use of a clip space between 0 to 1](#section2_17)
-+ [2.18. GLM\_FORCE\_SIZE\_T\_LENGTH: Vector and matrix static size type](#section2_18)
-+ [2.19. GLM\_FORCE\_UNRESTRICTED\_GENTYPE: Removing genType restriction](#section2_19)
-+ [2.20. GLM\_FORCE\_SILENT\_WARNINGS: Silent C++ warnings from language extensions](#section2_20)
-+ [2.21. GLM\_FORCE\_QUAT\_DATA\_WXYZ: Force GLM to store quat data as w,x,y,z instead of x,y,z,w](#section2_21)
-+ [3. Stable extensions](#section3)
-+ [3.1. Scalar types](#section3_1)
-+ [3.2. Scalar functions](#section3_2)
-+ [3.3. Vector types](#section3_3)
-+ [3.4. Vector types with precision qualifiers](#section3_4)
-+ [3.5. Vector functions](#section3_5)
-+ [3.6. Matrix types](#section3_6)
-+ [3.7. Matrix types with precision qualifiers](#section3_7)
-+ [3.8. Matrix functions](#section3_8)
-+ [3.9. Quaternion types](#section3_9)
-+ [3.10. Quaternion types with precision qualifiers](#section3_10)
-+ [3.11. Quaternion functions](#section3_11)
-+ [4. Recommended extensions](#section4)
-+ [4.1. GLM_GTC_bitfield](#section4_1)
-+ [4.2. GLM_GTC_color_space](#section4_2)
-+ [4.3. GLM_GTC_constants](#section4_3)
-+ [4.4. GLM_GTC_epsilon](#section4_4)
-+ [4.5. GLM_GTC_integer](#section4_5)
-+ [4.6. GLM_GTC_matrix_access](#section4_6)
-+ [4.7. GLM_GTC_matrix_integer](#section4_7)
-+ [4.8. GLM_GTC_matrix_inverse](#section4_8)
-+ [4.9. GLM_GTC_matrix_transform](#section4_9)
-+ [4.10. GLM_GTC_noise](#section4_10)
-+ [4.11. GLM_GTC_packing](#section4_11)
-+ [4.12. GLM_GTC_quaternion](#section4_12)
-+ [4.13. GLM_GTC_random](#section4_13)
-+ [4.14. GLM_GTC_reciprocal](#section4_14)
-+ [4.15. GLM_GTC_round](#section4_15)
-+ [4.16. GLM_GTC_type_alignment](#section4_16)
-+ [4.17. GLM_GTC_type_precision](#section4_17)
-+ [4.18. GLM_GTC_type_ptr](#section4_18)
-+ [4.19. GLM_GTC_ulp](#section4_19)
-+ [4.20. GLM_GTC_vec1](#section4_20)
-+ [5. OpenGL interoperability](#section5)
-+ [5.1. GLM Replacements for deprecated OpenGL functions](#section5_1)
-+ [5.2. GLM Replacements for GLU functions](#section5_2)
-+ [6. Known issues](#section6)
-+ [6.1. Not function](#section6_1)
-+ [6.2. Precision qualifiers support](#section6_2)
-+ [7. FAQ](#section7)
-+ [7.1 Why GLM follows GLSL specification and conventions?](#section7_1)
-+ [7.2. Does GLM run GLSL programs?](#section7_2)
-+ [7.3. Does a GLSL compiler build GLM codes?](#section7_3)
-+ [7.4. Should I use ‘GTX’ extensions?](#section7_4)
-+ [7.5. Where can I ask my questions?](#section7_5)
-+ [7.6. Where can I find the documentation of extensions?](#section7_6)
-+ [7.7. Should I use 'using namespace glm;'?](#section7_7)
-+ [7.8. Is GLM fast?](#section7_8)
-+ [7.9. When I build with Visual C++ with /w4 warning level, I have warnings...](#section7_9)
-+ [7.10. Why some GLM functions can crash because of division by zero?](#section7_10)
-+ [7.11. What unit for angles us used in GLM?](#section7_11)
-+ [7.12. Windows headers cause build errors...](#section7_12)
-+ [7.13. Constant expressions support](#section7_13)
-+ [8. Code samples](#section8)
-+ [8.1. Compute a triangle normal](#section8_1)
-+ [8.2. Matrix transform](#section8_2)
-+ [8.3. Vector types](#section8_3)
-+ [8.4. Lighting](#section8_4)
-+ [9. Contributing to GLM](#section9)
-+ [9.1. Submitting bug reports](#section9_1)
-+ [9.2. Contributing to GLM with pull request](#section9_2)
-+ [9.3. Coding style](#section9_3)
-+ [10. References](#section10)
-+ [10.1. OpenGL specifications](#section10_1)
-+ [10.2. External links](#section10_2)
-+ [10.3. Projects using GLM](#section10_3)
-+ [10.4. Tutorials using GLM](#section10_4)
-+ [10.5. Equivalent for other languages](#section10_5)
-+ [10.6. Alternatives to GLM](#section10_6)
-+ [10.7. Acknowledgements](#section10_7)
+
+* [0. Licenses](#section0)
+* [1. Getting started](#section1)
+* [1.1. Using global headers](#section1_1)
+* [1.2. Using separated headers](#section1_2)
+* [1.3. Using extension headers](#section1_3)
+* [1.4. Dependencies](#section1_4)
+* [1.5. Finding GLM with CMake](#section1_5)
+* [2. Preprocessor configurations](#section2)
+* [2.1. GLM\_FORCE\_MESSAGES: Platform auto detection and default configuration](#section2_1)
+* [2.2. GLM\_FORCE\_PLATFORM\_UNKNOWN: Force GLM to no detect the build platform](#section2_2)
+* [2.3. GLM\_FORCE\_COMPILER\_UNKNOWN: Force GLM to no detect the C++ compiler](#section2_3)
+* [2.4. GLM\_FORCE\_ARCH\_UNKNOWN: Force GLM to no detect the build architecture](#section2_4)
+* [2.5. GLM\_FORCE\_CXX\_UNKNOWN: Force GLM to no detect the C++ standard](#section2_5)
+* [2.6. GLM\_FORCE\_CXX**: C++ language detection](#section2_6)
+* [2.7. GLM\_FORCE\_EXPLICIT\_CTOR: Requiring explicit conversions](#section2_7)
+* [2.8. GLM\_FORCE\_INLINE: Force inline](#section2_8)
+* [2.9. GLM\_FORCE\_ALIGNED\_GENTYPES: Force GLM to enable aligned types](#section2_9)
+* [2.10. GLM\_FORCE\_DEFAULT\_ALIGNED\_GENTYPES: Force GLM to use aligned types by default](#section2_10)
+* [2.11. GLM\_FORCE\_INTRINSICS: Using SIMD optimizations](#section2_11)
+* [2.12. GLM\_FORCE\_PRECISION\_**: Default precision](#section2_12)
+* [2.13. GLM\_FORCE\_SINGLE\_ONLY: Removed explicit 64-bits floating point types](#section2_13)
+* [2.14. GLM\_FORCE\_SWIZZLE: Enable swizzle operators](#section2_14)
+* [2.15. GLM\_FORCE\_XYZW\_ONLY: Only exposes x, y, z and w components](#section2_15)
+* [2.16. GLM\_FORCE\_LEFT\_HANDED: Force left handed coordinate system](#section2_16)
+* [2.17. GLM\_FORCE\_DEPTH\_ZERO\_TO\_ONE: Force the use of a clip space between 0 to 1](#section2_17)
+* [2.18. GLM\_FORCE\_SIZE\_T\_LENGTH: Vector and matrix static size type](#section2_18)
+* [2.19. GLM\_FORCE\_UNRESTRICTED\_GENTYPE: Removing genType restriction](#section2_19)
+* [2.20. GLM\_FORCE\_SILENT\_WARNINGS: Silent C++ warnings from language extensions](#section2_20)
+* [2.21. GLM\_FORCE\_QUAT\_DATA\_WXYZ: Force GLM to store quat data as w,x,y,z instead of x,y,z,w](#section2_21)
+* [3. Stable extensions](#section3)
+* [3.1. Scalar types](#section3_1)
+* [3.2. Scalar functions](#section3_2)
+* [3.3. Vector types](#section3_3)
+* [3.4. Vector types with precision qualifiers](#section3_4)
+* [3.5. Vector functions](#section3_5)
+* [3.6. Matrix types](#section3_6)
+* [3.7. Matrix types with precision qualifiers](#section3_7)
+* [3.8. Matrix functions](#section3_8)
+* [3.9. Quaternion types](#section3_9)
+* [3.10. Quaternion types with precision qualifiers](#section3_10)
+* [3.11. Quaternion functions](#section3_11)
+* [4. Recommended extensions](#section4)
+* [4.1. GLM_GTC_bitfield](#section4_1)
+* [4.2. GLM_GTC_color_space](#section4_2)
+* [4.3. GLM_GTC_constants](#section4_3)
+* [4.4. GLM_GTC_epsilon](#section4_4)
+* [4.5. GLM_GTC_integer](#section4_5)
+* [4.6. GLM_GTC_matrix_access](#section4_6)
+* [4.7. GLM_GTC_matrix_integer](#section4_7)
+* [4.8. GLM_GTC_matrix_inverse](#section4_8)
+* [4.9. GLM_GTC_matrix_transform](#section4_9)
+* [4.10. GLM_GTC_noise](#section4_10)
+* [4.11. GLM_GTC_packing](#section4_11)
+* [4.12. GLM_GTC_quaternion](#section4_12)
+* [4.13. GLM_GTC_random](#section4_13)
+* [4.14. GLM_GTC_reciprocal](#section4_14)
+* [4.15. GLM_GTC_round](#section4_15)
+* [4.16. GLM_GTC_type_alignment](#section4_16)
+* [4.17. GLM_GTC_type_precision](#section4_17)
+* [4.18. GLM_GTC_type_ptr](#section4_18)
+* [4.19. GLM_GTC_ulp](#section4_19)
+* [4.20. GLM_GTC_vec1](#section4_20)
+* [5. OpenGL interoperability](#section5)
+* [5.1. GLM Replacements for deprecated OpenGL functions](#section5_1)
+* [5.2. GLM Replacements for GLU functions](#section5_2)
+* [6. Known issues](#section6)
+* [6.1. Not function](#section6_1)
+* [6.2. Precision qualifiers support](#section6_2)
+* [7. FAQ](#section7)
+* [7.1 Why GLM follows GLSL specification and conventions?](#section7_1)
+* [7.2. Does GLM run GLSL programs?](#section7_2)
+* [7.3. Does a GLSL compiler build GLM codes?](#section7_3)
+* [7.4. Should I use ‘GTX’ extensions?](#section7_4)
+* [7.5. Where can I ask my questions?](#section7_5)
+* [7.6. Where can I find the documentation of extensions?](#section7_6)
+* [7.7. Should I use 'using namespace glm;'?](#section7_7)
+* [7.8. Is GLM fast?](#section7_8)
+* [7.9. When I build with Visual C++ with /w4 warning level, I have warnings...](#section7_9)
+* [7.10. Why some GLM functions can crash because of division by zero?](#section7_10)
+* [7.11. What unit for angles us used in GLM?](#section7_11)
+* [7.12. Windows headers cause build errors...](#section7_12)
+* [7.13. Constant expressions support](#section7_13)
+* [8. Code samples](#section8)
+* [8.1. Compute a triangle normal](#section8_1)
+* [8.2. Matrix transform](#section8_2)
+* [8.3. Vector types](#section8_3)
+* [8.4. Lighting](#section8_4)
+* [9. Contributing to GLM](#section9)
+* [9.1. Submitting bug reports](#section9_1)
+* [9.2. Contributing to GLM with pull request](#section9_2)
+* [9.3. Coding style](#section9_3)
+* [10. References](#section10)
+* [10.1. OpenGL specifications](#section10_1)
+* [10.2. External links](#section10_2)
+* [10.3. Projects using GLM](#section10_3)
+* [10.4. Tutorials using GLM](#section10_4)
+* [10.5. Equivalent for other languages](#section10_5)
+* [10.6. Alternatives to GLM](#section10_6)
+* [10.7. Acknowledgements](#section10_7)
 
 ---
 <div style="page-break-after: always;"> </div>
@@ -170,6 +171,7 @@ SOFTWARE OR THE USE OR OTHER DEALINGS IN
 <div style="page-break-after: always;"> </div>
 
 ## <a name="section1"></a> 1. Getting started
+
 ### <a name="section1_1"></a> 1.1. Using global headers
 
 GLM is a header-only library, and thus does not need to be compiled. We can use GLM's implementation of GLSL's mathematics functionality by including the `<glm/glm.hpp>` header:
@@ -179,6 +181,7 @@ GLM is a header-only library, and thus d
 ```
 
 To extend the feature set supported by GLM and keeping the library as close to GLSL as possible, new features are implemented as extensions that can be included thought a separated header:
+
 ```cpp 
 // Include all GLM core / GLSL features
 #include <glm/glm.hpp> // vec2, vec3, mat4, radians
@@ -202,6 +205,7 @@ glm::mat4 transform(glm::vec2 const& Ori
 ### <a name="section1_2"></a> 1.2. Using separated headers
 
 GLM relies on C++ templates heavily, and may significantly increase compilation times for projects that use it. Hence, user projects could only include the features they actually use. Following is the list of all the core features, based on GLSL specification, headers:
+
 ```cpp
 #include <glm/vec2.hpp>               // vec2, bvec2, dvec2, ivec2 and uvec2
 #include <glm/vec3.hpp>               // vec3, bvec3, dvec3, ivec3 and uvec3
@@ -250,6 +254,7 @@ glm::mat4 transform(glm::vec2 const& Ori
 ### <a name="section1_3"></a> 1.3. Using extension headers
 
 Using GLM through split headers to minimize the project build time:
+
 ```cpp
 // Include GLM vector extensions:
 #include <glm/ext/vector_float2.hpp>                // vec2
@@ -300,7 +305,8 @@ Using `GLM_FORCE_MESSAGES`, GLM will rep
 ```
 
 Example of configuration log generated by `GLM_FORCE_MESSAGES`:
-```cpp
+
+```plaintext
 GLM: version 0.9.9.1
 GLM: C++ 17 with extensions
 GLM: Clang compiler detected
@@ -342,6 +348,7 @@ any inclusion of `<glm/glm.hpp>` to rest
 ```
 
 For C++11, C++14, and C++17 equivalent defines are available:
+
 * `GLM_FORCE_CXX11`
 * `GLM_FORCE_CXX14`
 * `GLM_FORCE_CXX17`
@@ -722,7 +729,6 @@ int average(int const A, int const B)
 When using /W4 on Visual C++ or -Wpedantic on GCC, for example, the compilers will generate warnings for using C++ language extensions (/Za with Visual C++) such as anonymous struct.
 GLM relies on anonymous structs for swizzle operators and aligned vector types. To silent those warnings define `GLM_FORCE_SILENT_WARNINGS` before including GLM headers.
 
-
 ### <a name="section2_21"></a> 2.21. GLM\_FORCE\_QUAT\_DATA\_WXYZ: Force GLM to store quat data as w,x,y,z instead of x,y,z,w
 
 By default GLM store quaternion components with the x, y, z, w order. `GLM_FORCE_QUAT_DATA_WXYZ` allows switching the quaternion data storage to the w, x, y, z order.
@@ -1005,7 +1011,7 @@ This extension exposes double-precision
 
 Include `<glm/ext/vector_double4_precision.hpp>` to use these features.
 
-### <a name="section3_4"></a> 3.5. Vector functions
+### <a name="section3_5"></a> 3.5. Vector functions
 
 #### 3.5.1. GLM_EXT_vector_common
 
@@ -1500,41 +1506,41 @@ projective matrix functions (`perspectiv
 
 Define 2D, 3D and 4D procedural noise functions.
 
-<`glm/gtc/noise.hpp>` need to be included to use these features.
+`<glm/gtc/noise.hpp>` need to be included to use these features.
 
-![](/doc/manual/noise-simplex1.jpg)
+![](./doc/manual/noise-simplex1.jpg)
 
 Figure 4.10.1: glm::simplex(glm::vec2(x / 16.f, y / 16.f));
 
-![](/doc/manual/noise-simplex2.jpg)
+![](./doc/manual/noise-simplex2.jpg)
 
 Figure 4.10.2: glm::simplex(glm::vec3(x / 16.f, y / 16.f, 0.5f));
 
-![](/doc/manual/noise-simplex3.jpg)
+![](./doc/manual/noise-simplex3.jpg)
 
 Figure 4.10.3: glm::simplex(glm::vec4(x / 16.f, y / 16.f, 0.5f, 0.5f));
 
-![](/doc/manual/noise-perlin1.jpg)
+![](./doc/manual/noise-perlin1.jpg)
 
 Figure 4.10.4: glm::perlin(glm::vec2(x / 16.f, y / 16.f));
 
-![](/doc/manual/noise-perlin2.jpg)
+![](./doc/manual/noise-perlin2.jpg)
 
 Figure 4.10.5: glm::perlin(glm::vec3(x / 16.f, y / 16.f, 0.5f));
 
-![](/doc/manual/noise-perlin3.jpg)
+![](./doc/manual/noise-perlin3.jpg)
 
 Figure 4.10.6: glm::perlin(glm::vec4(x / 16.f, y / 16.f, 0.5f, 0.5f)));
 
-![](/doc/manual/noise-perlin4.png)
+![](./doc/manual/noise-perlin4.png)
 
 Figure 4.10.7: glm::perlin(glm::vec2(x / 16.f, y / 16.f), glm::vec2(2.0f));
 
-![](/doc/manual/noise-perlin5.png)
+![](./doc/manual/noise-perlin5.png)
 
 Figure 4.10.8: glm::perlin(glm::vec3(x / 16.f, y / 16.f, 0.5f), glm::vec3(2.0f));
 
-![](/doc/manual/noise-perlin6.png)
+![](./doc/manual/noise-perlin6.png)
 
 Figure 4.10.9: glm::perlin(glm::vec4(x / 16.f, y / 16.f, glm::vec2(0.5f)), glm::vec4(2.0f));
 
@@ -1556,27 +1562,27 @@ Probability distributions in up to four
 
 `<glm/gtc/random.hpp>` need to be included to use these features.
 
-![](/doc/manual/random-linearrand.png)
+![](./doc/manual/random-linearrand.png)
 
 Figure 4.13.1: glm::vec4(glm::linearRand(glm::vec2(-1), glm::vec2(1)), 0, 1);
 
-![](/doc/manual/random-circularrand.png)
+![](./doc/manual/random-circularrand.png)
 
 Figure 4.13.2: glm::vec4(glm::circularRand(1.0f), 0, 1);
 
-![](/doc/manual/random-sphericalrand.png)
+![](./doc/manual/random-sphericalrand.png)
 
 Figure 4.13.3: glm::vec4(glm::sphericalRand(1.0f), 1);
 
-![](/doc/manual/random-diskrand.png)
+![](./doc/manual/random-diskrand.png)
 
 Figure 4.13.4: glm::vec4(glm::diskRand(1.0f), 0, 1);
 
-![](/doc/manual/random-ballrand.png)
+![](./doc/manual/random-ballrand.png)
 
 Figure 4.13.5: glm::vec4(glm::ballRand(1.0f), 1);
 
-![](/doc/manual/random-gaussrand.png)
+![](./doc/manual/random-gaussrand.png)
 
 Figure 4.13.6: glm::vec4(glm::gaussRand(glm::vec3(0), glm::vec3(1)), 1);
 
@@ -1602,7 +1608,7 @@ Aligned vector types.
 
 Vector and matrix types with defined precisions, e.g. `i8vec4`, which is a 4D vector of signed 8-bit integers.
 
-`<glm/gtc/type\_precision.hpp>` need to be included to use the features of this extension.
+`<glm/gtc/type_precision.hpp>` need to be included to use the features of this extension.
 
 ### <a name="section4_18"></a> 4.18. GLM\_GTC\_type\_ptr
 
@@ -1638,7 +1644,7 @@ void foo()
 ```
 
 *Note: It would be possible to implement [`glVertex3fv`](http://www.opengl.org/sdk/docs/man2/xhtml/glVertex.xml)(glm::vec3(0)) in C++ with the appropriate cast operator that would result as an
-implicit cast in this example. However cast operators may produce programs running with unexpected behaviours without build error or any form of notification. *
+implicit cast in this example. However cast operators may produce programs running with unexpected behaviours without build error or any form of notification.*
 
 `<glm/gtc/type_ptr.hpp>` need to be included to use these features.
 
@@ -2072,7 +2078,8 @@ Additional, bugs can be configuration sp
 ```
 
 An example of build messages generated by GLM:
-```
+
+```plaintext
 GLM: 0.9.9.1
 GLM: C++ 17 with extensions
 GLM: GCC compiler detected"
@@ -2095,10 +2102,10 @@ The tutorial assumes you have some basic
 
 #### Step 1: Setup our GLM Fork
 
-We will make our changes in our own copy of the GLM sitory. On the GLM GitHub repo and we press the Fork button.
+We will make our changes in our own copy of the GLM repository. On the GLM GitHub repo and we press the Fork button.
 We need to download a copy of our fork to our local machine. In the terminal, type:
 
-```
+```plaintext
 >>> git clone <our-repository-fork-git-url>
 ```
 
@@ -2110,25 +2117,25 @@ We can find our repository git url on th
 
 We can use the following command to add `upstream` (original project repository) as a remote repository so that we can fetch the latest GLM commits into our branch and keep our forked copy is synchronized.
 
-```
+```plaintext
 >>> git remote add upstream https://github.com/processing/processing.git
 ```
 
 To synchronize our fork to the latest commit in the GLM repository, we can use the following command:
 
-```
+```plaintext
 >>> git fetch upstream
 ```
 
 Then, we can merge the remote master branch to our current branch:
 
-```
+```plaintext
 >>> git merge upstream/master
 ```
 
 Now our local copy of our fork has been synchronized. However, the fork's copy is not updated on GitHub's servers yet. To do that, use:
 
-```
+```plaintext
 >>> git push origin master
 ```
 
@@ -2141,16 +2148,19 @@ It's a good practice to make changes in
 Before creating a new branch, it's best to synchronize our fork and then create a new branch from the latest master branch.
 
 If we are not on the master branch, we should switch to it using:
+
 ```
 >>> git checkout master
 ```
 
 To create a new branch called `bugifx`, we use:
+
 ```
 git branch bugfix
 ```
 
 Once the code changes for the fix is done, we need to commit the changes:
+
 ```
 >>> git commit -m "Resolve the issue that caused problem with a specific fix #432"
 ```
@@ -2158,11 +2168,13 @@ Once the code changes for the fix is don
 The commit message should be as specific as possible and finished by the bug number in the [GLM GitHub issue page](https://github.com/g-truc/glm/issues)
 
 Finally, we need to push our changes in our branch to our GitHub fork using:
+
 ```
 >>> git push origin bugfix
 ```
 
 Some things to keep in mind for a pull request:
+
 * Keep it minimal: Try to make the minimum required changes to fix the issue. If we have added any debugging code, we should remove it.
 * A fix at a time: The pull request should deal with one issue at a time only, unless two issue are so interlinked they must be fixed together.
 * Write a test: GLM is largely unit tests. Unit tests are in `glm/test` directory. We should also add tests for the fixes we provide to ensure future regression doesn't happen.
@@ -2206,6 +2218,7 @@ else
 ```
 
 Single line if blocks:
+
 ```cpp
 if(blah)
     // yes like this
@@ -2214,6 +2227,7 @@ else
 ```
 
 No spaces inside parens:
+
 ```cpp
 if (blah)    // No
 if( blah )   // No
@@ -2222,12 +2236,14 @@ if(blah)     // Yes
 ```
 
 Use spaces before/after commas:
+
 ```cpp
 someFunction(apple,bear,cat);     // No
 someFunction(apple, bear, cat);   // Yes
 ```
 
 Use spaces before/after use of `+, -, *, /, %, >>, <<, |, &, ^, ||, &&` operators:
+
 ```cpp
 vec4 v = a + b;
 ```
@@ -2279,7 +2295,7 @@ namespace detail // glm::detail namespac
 
 * OpenGL 4.3 core specification
 * [GLSL 4.30 specification](http://www.opengl.org/registry/doc/GLSLangSpec.4.30.7.diff.pdf)
-![](media/image21.png){width="2.859722222222222in" height="1.6083333333333334in"}- [*GLU 1.3 specification*](http://www.opengl.org/documentation/specs/glu/glu1_3.pdf)
+* [GLU 1.3 specification](http://www.opengl.org/documentation/specs/glu/glu1_3.pdf)
 
 ### <a name="section10_2"></a> 10.2. External links
 
@@ -2299,9 +2315,9 @@ Beautifully hand-crafted levels bring th
 
 “Whatever lies ahead, I must recover my fortune.” -Leopold
 
-![](/doc/manual/references-leosfortune.jpeg)
+![](./doc/manual/references-leosfortune.jpeg)
 
-![](/doc/manual/references-leosfortune2.jpg)
+![](./doc/manual/references-leosfortune2.jpg)
 
 [***OpenGL 4.0 Shading Language Cookbook***](http://www.packtpub.com/opengl-4-0-shading-language-cookbook/book?tag=rk/opengl4-abr1/0811)
 
@@ -2313,19 +2329,19 @@ A set of recipes that demonstrates a wid
 
 Simple, easy-to-follow examples with GLSL source code are provided, as well as a basic description of the theory behind each technique.
 
-![](/doc/manual/references-glsl4book.jpg)
+![](./doc/manual/references-glsl4book.jpg)
 
 [***Outerra***](http://outerra.com/)
 
 A 3D planetary engine for seamless planet rendering from space down to the surface. Can use arbitrary resolution of elevation data, refining it to centimetre resolution using fractal algorithms.
 
-![](/doc/manual/references-outerra1.jpg)
+![](./doc/manual/references-outerra1.jpg)
 
-![](/doc/manual/references-outerra2.jpg)
+![](./doc/manual/references-outerra2.jpg)
 
-![](/doc/manual/references-outerra3.jpg)
+![](./doc/manual/references-outerra3.jpg)
 
-![](/doc/manual/references-outerra4.jpg)
+![](./doc/manual/references-outerra4.jpg)
 
 [***Falcor***](https://github.com/NVIDIA/Falcor)
 
@@ -2339,7 +2355,7 @@ Cinder is a C++ library for programming
 
 Cinder is production-proven, powerful enough to be the primary tool for professionals, but still suitable for learning and experimentation. Cinder is released under the [2-Clause BSD License](http://opensource.org/licenses/BSD-2-Clause).
 
-![](/doc/manual/references-cinder.png)
+![](./doc/manual/references-cinder.png)
 
 [***opencloth***](http://code.google.com/p/opencloth/)
 
@@ -2347,9 +2363,9 @@ A collection of source codes implementin
 
 Simple, easy-to-follow examples with GLSL source code, as well as a basic description of the theory behind each technique.
 
-![](/doc/manual/references-opencloth1.png)
+![](./doc/manual/references-opencloth1.png)
 
-![](/doc/manual/references-opencloth3.png)
+![](./doc/manual/references-opencloth3.png)
 
 [***LibreOffice***](https://www.libreoffice.org/)
 
@@ -2400,6 +2416,7 @@ LibreOffice includes several application
 GLM is developed and maintained by [*Christophe Riccio*](http://www.g-truc.net) but many contributors have made this project what it is.
 
 Special thanks to:
+
 * Ashima Arts and Stefan Gustavson for their work on [*webgl-noise*](https://github.com/ashima/webgl-noise) which has been used for GLM noises implementation.
 * [*Arthur Winters*](http://athile.net/library/wiki/index.php?title=Athile_Technologies) for the C++11 and Visual C++ swizzle operators implementation and tests.
 * Joshua Smith and Christoph Schied for the discussions and the experiments around the swizzle operators implementation issues.
